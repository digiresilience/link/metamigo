#!/bin/bash

set -euo pipefail

DEP=${1:-}
VERSION=${2:-}

if [[ -z "$DEP" || -z "$VERSION" ]]; then
  echo "usage: $0 <package name> <new version>"
  echo
  echo "example:"
  echo "  $0 @package/name 0.1.1"
  echo
  exit 1
fi

APPS=$(ls -1 @app/)


for app in $APPS; do
  grep --silent "\"$DEP\"" @app/$app/package.json
  if [[ "$?" == 0 ]]; then
    yarn workspace "@app/$app" upgrade $DEP@$VERSION
  fi
done

