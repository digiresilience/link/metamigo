FROM node:14-buster as builder

ARG AMIGO_DIR=/opt/metamigo
RUN mkdir -p ${AMIGO_DIR}/
WORKDIR ${AMIGO_DIR}
# we copy the existing node modules (if any) to speed up the initial build
# COPY cli package.json yarn.lock tsconfig.json tsconfig.base.json node_modules @app ${AMIGO_DIR}/
COPY .npmrc cli package.json yarn.lock tsconfig.json tsconfig.base.json ${AMIGO_DIR}/
# COPY node_modules ${AMIGO_DIR}/node_modules
COPY @app ${AMIGO_DIR}/@app

RUN --mount=type=cache,target=/yarn \
    YARN_CACHE_FOLDER=/yarn yarn install \
    --frozen-lockfile --production=false --no-progress \
    --prefer-offline
RUN npx --no-install tsc --build --verbose
ENV NEXT_TELEMETRY_DISABLED=1
RUN yarn workspace @app/frontend build

RUN rm -Rf ./node_modules ./@app/*/node_modules

FROM node:14-buster as clean
ARG AMIGO_DIR=/opt/metamigo

# now we copy over just what we need for production and ignore the rest

COPY --from=builder ${AMIGO_DIR}/cli ${AMIGO_DIR}/package.json ${AMIGO_DIR}/yarn.lock ${AMIGO_DIR}/

COPY --from=builder ${AMIGO_DIR}/@app/config/ ${AMIGO_DIR}/@app/config/
COPY --from=builder ${AMIGO_DIR}/@app/db/ ${AMIGO_DIR}/@app/db/

COPY --from=builder ${AMIGO_DIR}/@app/frontend/.next ${AMIGO_DIR}/@app/frontend/.next
COPY --from=builder ${AMIGO_DIR}/@app/frontend/package.json ${AMIGO_DIR}/@app/frontend/package.json
COPY --from=builder ${AMIGO_DIR}/@app/frontend/next.config.js ${AMIGO_DIR}/@app/frontend/next.config.js

COPY --from=builder ${AMIGO_DIR}/@app/api/build ${AMIGO_DIR}/@app/api/build
COPY --from=builder ${AMIGO_DIR}/@app/api/package.json ${AMIGO_DIR}/@app/api/package.json

COPY --from=builder ${AMIGO_DIR}/@app/worker/build ${AMIGO_DIR}/@app/worker/build
COPY --from=builder ${AMIGO_DIR}/@app/worker/package.json ${AMIGO_DIR}/@app/worker/package.json

COPY --from=builder ${AMIGO_DIR}/@app/cli/build ${AMIGO_DIR}/@app/cli/build
COPY --from=builder ${AMIGO_DIR}/@app/cli/package.json ${AMIGO_DIR}/@app/cli/package.json

RUN rm -Rf ./node_modules ./@app/*/node_modules

FROM node:14-buster as pristine
LABEL maintainer="Abel Luck <abel@guardianproject.info>"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y --no-install-recommends --fix-missing \
    postgresql-client dumb-init ffmpeg


ARG AMIGO_DIR=/opt/metamigo
ENV AMIGO_DIR ${AMIGO_DIR}
RUN mkdir -p ${AMIGO_DIR}
RUN chown -R node:node ${AMIGO_DIR}/

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

COPY --from=clean ${AMIGO_DIR}/ ${AMIGO_DIR}/

WORKDIR ${AMIGO_DIR}

RUN --mount=type=cache,target=/yarn \
    YARN_CACHE_FOLDER=/yarn yarn install \
    --frozen-lockfile --production=false --no-progress \
    --prefer-offline

USER node

EXPOSE 3000
EXPOSE 3001
EXPOSE 3002
ENV PORT 3000
ENV NODE_ENV production

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL="https://gitlab.com/digiresilience/link/metamigo"
ARG VERSION
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="digiresilience.org/link/metamigo"
LABEL org.label-schema.description="part of CDR Link"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION

ENTRYPOINT ["/docker-entrypoint.sh"]
