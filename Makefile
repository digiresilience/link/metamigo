PACKAGE_NAME ?= $(shell jq -r '.name' package.json)
PACKAGE_VERSION?= $(shell jq -r '.version' package.json)
BUILD_DATE   ?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
DOCKER_ARGS  ?=
DOCKER_NS    ?= registry.gitlab.com/digiresilience/link/${PACKAGE_NAME}
DOCKER_TAG   ?= test
DOCKER_BUILD := docker build ${DOCKER_ARGS} --build-arg BUILD_DATE=${BUILD_DATE}
DOCKER_BUILD_FRESH := ${DOCKER_BUILD} --pull --no-cache
DOCKER_BUILD_ARGS := --build-arg VCS_REF=${CI_COMMIT_SHORT_SHA}
DOCKER_PUSH  := docker push
DOCKER_BUILD_TAG := ${DOCKER_NS}:${DOCKER_TAG}
PACKAGES := $(shell ls @app)
BUILD := $(patsubst %, %/build, $(PACKAGES))
LINT := $(patsubst %, %/lint, $(PACKAGES))
FMT := $(patsubst %, %/fmt, $(PACKAGES))
NODE_MODULES_CLEAN:= $(patsubst %, @app/%/node-modules, $(PACKAGES))
CLEAN_PATTERNS := coverage yarn-error.log build tmp junit.xml .next
CLEAN := $(foreach DIR,$(PACKAGES),$(addprefix @app/$(DIR)/,$(CLEAN_PATTERNS))) @app/e2e/cypress/screenshots @app/e2e/cypress/videos
NODE_ENV ?= development
AMIGO_CONFIG ?= ${PWD}/.amigo.local.json
CURRENT_UID := $(shell id -u):$(shell id -g)

.PHONY: build test node_modules/ .npmrc
.EXPORT_ALL_VARIABLES:


build: node_modules/ husky
	npx --no-install tsc --build --verbose
watch:
	npx --no-install tsc --build --verbose --watch

list:
	@ls -1 @app

# Use make <app>/build to build just a single project
${BUILD}: node_modules/
	yarn workspace @app/${@D} build

${LINT}:
	yarn workspace @app/${@D} lint:lint
	yarn workspace @app/${@D} lint:prettier

${FMT}:
	yarn workspace @app/${@D} fix:prettier
	yarn workspace @app/${@D} fix:lint

fmt: ${FMT}

test: AMIGO_CONFIG=${PWD}/.test.local.json
test: config/test db/test api/test worker/test cli/test frontend/test e2e/test

cypress:
	@echo "Be patient, this could take ~30s-1 minute"
	yarn workspace @app/e2e open

tunnel: AMIGO_CONFIG=${PWD}/.amigo.local.json
tunnel:
	cloudflared tunnel --hostname $(shell jq -r '.frontend.url|capture("https?://(?<hostname>.*)").hostname' $(AMIGO_CONFIG)) --url http://localhost:2999

signald:
	mkdir -p signald

api: AMIGO_CONFIG=${PWD}/.amigo.local.json
api: build db/start db/current
	NODE_ENV=development npx --no-install nodemon \
	--watch @app/cli/build \
	--watch @app/api/build \
	--watch @app/db/build \
	--watch @app/worker/build \
	--watch @app/config/build \
	--unhandled-rejections=strict @app/cli/build/main/index.js api

worker: AMIGO_CONFIG=${PWD}/.amigo.local.json
worker: build db/start db/current
	NODE_ENV=development npx --no-install nodemon \
	--watch @app/cli/build \
	--watch @app/api/build \
	--watch @app/db/build \
	--watch @app/worker/build \
	--watch @app/config/build \
	--unhandled-rejections=strict @app/cli/build/main/index.js worker

@app/frontend/src/lib/graphql-schema.json:  @app/db/migrations/current.sql
	./cli export-graphql-schema

frontend: AMIGO_CONFIG=${PWD}/.amigo.local.json
frontend: build @app/frontend/src/lib/graphql-schema.json
	yarn workspace @app/$@ dev

api/test: AMIGO_CONFIG=${PWD}/.test.local.json
api/test: build db/start
	@echo "TESTING: ${@D} with config ${AMIGO_CONFIG}"
	@yarn workspace @app/api test

api/watch-test: AMIGO_CONFIG=${PWD}/.test.local.json
api/watch-test: build db/start
	@yarn workspace @app/api watch:test

db/test: AMIGO_CONFIG=${PWD}/.test.local.json
db/test: build
	@echo "TESTING: ${@D}"
	@yarn workspace @app/${@D} test

config/test: AMIGO_CONFIG=${PWD}/.test.local.json
config/test: build
	@echo "TESTING: ${@D}"
	@yarn workspace @app/${@D} test

worker/test: AMIGO_CONFIG=${PWD}/.test.local.json
worker/test: build
	@echo "TESTING: ${@D}"
	@yarn workspace @app/${@D} test

worker/watch-test: AMIGO_CONFIG=${PWD}/.test.local.json
worker/watch-test: build db/start
	@yarn workspace @app/worker watch:test

frontend/test: AMIGO_CONFIG=${PWD}/.test.local.json
frontend/test: build
	@echo "TESTING: ${@D}"
	@yarn workspace @app/${@D} test

cli/test: AMIGO_CONFIG=${PWD}/.test.local.json
cli/test: build
	@echo "TESTING: ${@D}"
	@yarn workspace @app/${@D} test

e2e/test: AMIGO_CONFIG=${PWD}/.test.local.json
e2e/test: @app/frontend/src/lib/graphql-schema.json
	@echo "TESTING: ${@D}"
	yarn workspace @app/e2e test

e2e/chrome: AMIGO_CONFIG=${PWD}/.test.local.json
e2e/chrome:
	yarn workspace @app/e2e test:chrome

e2e/chromium: AMIGO_CONFIG=${PWD}/.test.local.json
e2e/chromium:
	yarn workspace @app/e2e test:chromium

e2e/firefox: AMIGO_CONFIG=${PWD}/.test.local.json
e2e/firefox:
	yarn workspace @app/e2e test:firefox

db/prepare-docker-test:
ifndef CI_JOB_TOKEN
	@CURRENT_UID=$(CURRENT_UID) docker-compose rm --force --stop -v db_test
	@CURRENT_UID=$(CURRENT_UID) docker-compose up --remove-orphans --detach db_test
endif

db/reset-docker:
ifndef CI_JOB_TOKEN
	CURRENT_UID=$(CURRENT_UID) docker-compose down -v
	CURRENT_UID=$(CURRENT_UID) docker-compose up --remove-orphans --build  --detach
	sleep 1
endif

db/start: signald
ifndef CI_JOB_TOKEN
	@if [ -z "$(shell CURRENT_UID=$(CURRENT_UID) docker-compose ps  --filter status=running --services)" ]; then \
	 CURRENT_UID=$(CURRENT_UID) docker-compose up --remove-orphans --build  --detach && \
	sleep 2; \
	fi
endif

db/up: signald
	CURRENT_UID=$(CURRENT_UID) docker-compose up -d

db/kill:
	CURRENT_UID=$(CURRENT_UID) docker-compose kill

db/restart: signald
	CURRENT_UID=$(CURRENT_UID) docker-compose restart

db/stop:
	CURRENT_UID=$(CURRENT_UID) docker-compose stop

db/down:
	CURRENT_UID=$(CURRENT_UID) docker-compose down -v --rmi local

db/current:
	./cli db -- watch --once

db/watch:
	./cli db -- watch

db/commit:
	./cli db -- commit

db/uncommit:
	./cli db -- uncommit

db/reset:
	sleep 1
	./cli db -- reset --erase
	./cli db -- watch --once



node_modules/: .npmrc
ifndef CI_JOB_TOKEN
	(test -d node_modules) || yarn
else
	# always run yarn when we are in the CI
	yarn install --cache-folder .yarn
endif

.npmrc:
ifdef CI_JOB_TOKEN
	echo '@guardianproject-ops:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
	echo '@digiresilience:registry=https://gitlab.com/api/v4/packages/npm/' >> .npmrc
	echo '//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
	echo '//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}' >> .npmrc
endif

husky: .npmrc node_modules/
ifndef CI_JOB_TOKEN
	@grep -q husky .git/config || npx --no-install husky install
endif

clean:
	rm -rf ${CLEAN}
	CURRENT_UID=$(CURRENT_UID) docker-compose down -v

distclean: clean
	rm -rf node_modules ${NODE_MODULES_CLEAN} signald

docker/build: .npmrc
	DOCKER_BUILDKIT=1 ${DOCKER_BUILD} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

docker/build-fresh: .npmrc
	DOCKER_BUILDKIT=1 ${DOCKER_BUILD_FRESH} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

docker/add-tag:
	docker pull ${DOCKER_NS}:${DOCKER_TAG}
	docker tag ${DOCKER_NS}:${DOCKER_TAG} ${DOCKER_NS}:${DOCKER_TAG_NEW}
	docker push ${DOCKER_NS}:${DOCKER_TAG_NEW}

docker/push:
	${DOCKER_PUSH} ${DOCKER_BUILD_TAG}

docker/build-push: docker/build docker/push
docker/build-fresh-push: docker/build-fresh docker/push
