# metamigo

> i want it all
> -- Freddie Mercury

## Developer Documentation

This project is built on top of [Amigo][amigo-starter], the opinionated application stack from [CDR][cdrtech].

There is a lot here. Follow the quick start below, and then refer to the in-depth documentation in the [amigo-starter README][amigo-starter]

### Quickstart

```console
git clone git@gitlab.com:digiresilience/link/metamigo.git
cd metamigo
yarn
make build

# run the api backend
make api

# run the frontend
make frontend

# run entire test suite
make test
```

NOTE: For the e2e test suite to run you'll need the frontend and api server
running in the background (using & or tmux or screen)

[amigo-starter]: https://gitlab.com/digiresilience/link/amigo-starter/

## Credits

Copyright © 2021-present [Center for Digital Resilience][cdr]

### Contributors

| [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] | [![Darren Clarke][darren_avatar]][darren_homepage]<br/>[Darren Clarke][darren_homepage] |
| ---------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |

[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&d=identicon
[darren_homepage]: https://github.com/redaranj
[darren_avatar]: https://s.gravatar.com/avatar/a4bb148b2c1a615f4b77b1e192f8e410?s=100

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
