require('@digiresilience/eslint-config-amigo/patch/modern-module-resolution');
module.exports = {
  extends: [
    "@digiresilience/eslint-config-amigo/profile/node",
    "@digiresilience/eslint-config-amigo/profile/typescript"
  ],
  parserOptions: { tsconfigRootDir: __dirname },
  rules: {
    "import/no-extraneous-dependencies": [
      // enable this when  this is fixed
      // https://github.com/benmosher/eslint-plugin-import/pull/1696
      "off",
      { packageDir: [".", "node_modules/@digiresilience/amigo", "node_modules/@digiresilience/amigo-dev"] },
    ],
    // TODO: enable this after jest fixes this issue https://github.com/nodejs/node/issues/38343
    "unicorn/prefer-node-protocol": "off"
  }
};
