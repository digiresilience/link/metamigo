/// <reference types="Cypress" />
type Chainable<Subject = any> = Cypress.Chainable<Subject>;

function getCy(cyName: string): Chainable<JQuery<HTMLElement>> {
  return cy.get(`[data-cy=${cyName}]`);
}

export {}; // Make this a module so we can `declare global`

declare global {
  namespace Cypress {
    interface Chainable {
      getCy: typeof getCy;
    }
  }
}
