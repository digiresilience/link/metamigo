/* eslint-disable unicorn/prefer-module */
/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

import { loadConfig } from "@app/config";

/**
 * @type {Cypress.PluginConfig}
 * @param on is used to hook into various events Cypress emits
 * @param config is the resolved Cypress config
 */
module.exports = async (on, config) => {
  const appConfig = await loadConfig();
  config.baseUrl = appConfig.frontend.url;
  return config;
};
