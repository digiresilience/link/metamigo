/// <reference types="Cypress" />

context("HomePage", () => {
  it("renders correctly", () => {
    // Setup
    cy.visit("/admin", { timeout: 120000 });

    // Action

    // Assertions
    // cy.url().should("equal", Cypress.config("baseUrl") + "/");
    cy.get("form", { timeout: 20000 }).within(() => {
      cy.get("button").should("exist").contains("Sign in with GitLab");
    });
  });
});
