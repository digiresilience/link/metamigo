import config from "./config";
import {
  readyConfig,
  resetConfig,
  initServer,
  resetServer,
  resetDb,
  AUTH_ADMIN,
} from "./helpers.test";
// import { readyDb, resetDb } from "./helpers.test";

beforeAll(() => resetDb());

afterAll(() => resetDb());

describe("Config", () => {
  beforeEach(readyConfig);

  afterEach(resetConfig);

  test("db name", async () => {
    expect(config.db.name).toBe("amigo_test");
  });
});

describe("API Endpoints", () => {
  let server;
  let hapi;
  let basicAuth;

  const basicHeader = (secret) =>
    "Basic " + Buffer.from(secret + ":", "utf8").toString("base64");

  const headers = { authorization: AUTH_ADMIN };

  beforeEach(async () => {
    server = await initServer();
    hapi = server.hapiServer;

    basicAuth = { authorization: basicHeader(config.nextAuth.secret) };
  });

  afterEach(async () => resetServer(server));

  test("GET /status/ping", async () => {
    const { statusCode, result } = await hapi.inject({
      method: "get",
      url: "/status/ping",
    });
    expect(statusCode).toBe(200);
    expect(result).toBe("OK");
  });

  test("GET /status/inc", async () => {
    const { statusCode } = await hapi.inject({
      method: "get",
      url: "/status/inc",
    });
    expect(statusCode).toBe(200);
  });

  test("GET /api-docs", async () => {
    const { statusCode } = await hapi.inject({
      method: "get",
      url: "/api-docs",
    });
    expect(statusCode).toBe(200);
  });

  test("GET /api/nextauth", async () => {
    const { result1 } = await hapi.inject({
      method: "get",
      url: "/api/nextauth/getUserByEmail/abel@guardianproject.info",
      headers: basicAuth,
    });
    expect(result1).toBeFalsy();

    const { statusCode: code2, result: abel } = await hapi.inject({
      method: "post",
      url: "/api/nextauth/createUser",
      headers: basicAuth,
      payload: {
        name: "Abel",
        email: "abel@guardianproject.info",
        userRole: "admin",
        createdBy: "testing",
      },
    });
    expect(code2).toBe(200);
    expect(abel.email).toBe("abel@guardianproject.info");

    const { statusCode: code3 } = await hapi.inject({
      method: "put",
      url: "/api/nextauth/linkAccount",
      headers: basicAuth,
      payload: {
        userId: abel.id,
        providerId: "providerA",
        providerType: "providerType",
        providerAccountId: "abel",
      },
    });
    expect(code3).toBe(204);

    const { statusCode: code4, result: result4 } = await hapi.inject({
      method: "get",
      url: "/api/nextauth/getUserByEmail/abel@guardianproject.info",
      headers: basicAuth,
    });
    const { email, name, userRole } = result4;

    expect(code4).toBe(200);
    expect(email).toBe("abel@guardianproject.info");
    expect(name).toBe("Abel");
    expect(userRole).toBe("admin");
  });

  test("GET /api/users", async () => {
    const { statusCode, result } = await hapi.inject({
      method: "get",
      url: "/api/users",
      headers,
    });

    expect(statusCode).toBe(200);
    expect(result.data).toHaveLength(1);
    const { email, name, userRole } = result.data[0];
    expect(email).toBe("abel@guardianproject.info");
    expect(name).toBe("Abel");
    expect(userRole).toBe("admin");
  });

  test("GET /api/number", async () => {
    const { statusCode, result } = await hapi.inject({
      method: "get",
      url: "/api/number",
      headers,
    });

    expect(statusCode).toBe(200);
    expect(result.result).toBe(42);
  });

  test("GET /api/string", async () => {
    const { statusCode, result } = await hapi.inject({
      method: "get",
      url: "/api/string",
      headers,
    });

    expect(statusCode).toBe(200);
    expect(result.result).toBe("@app/api is under development");
  });
});
