import * as Amigo from "@digiresilience/amigo";
import { defState } from "@digiresilience/montar";
import Manifest from "./manifest";
import config, { IAppConfig } from "../config";

export const deployment = async (
  config: IAppConfig,
  start = false
): Promise<Amigo.Server> => {
  // Build the manifest, which describes all the plugins needed for our application server
  const manifest = await Manifest.build(config);

  // Create the server and optionally start it
  const server = Amigo.deployment(manifest, config, start);

  return server;
};

export const stopDeployment = async (server: Amigo.Server): Promise<void> => {
  return Amigo.stopDeployment(server);
};

const server = defState("server", {
  start: () => deployment(config, true),
  stop: () => stopDeployment(server),
});

export default server;
