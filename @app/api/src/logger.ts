import { defState } from "@digiresilience/montar";
import { configureLogger } from "@digiresilience/amigo";
import config from "@app/config";

export const logger = defState("apiLogger", {
  start: async () => configureLogger(config),
});
export default logger;
