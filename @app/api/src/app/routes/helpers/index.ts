import * as Amigo from "@digiresilience/amigo";
import Toys from "@hapipal/toys";

export const withDefaults = Toys.withRouteDefaults({
  options: {
    cors: true,
    auth: "nextauth-jwt",
    validate: {
      failAction: Amigo.validatingFailAction,
    },
  },
});

export const noAuth = Toys.withRouteDefaults({
  options: {
    cors: true,
    validate: {
      failAction: Amigo.validatingFailAction,
    },
  },
});
