import * as Schmervice from "@hapipal/schmervice";

export default class RandomService extends Schmervice.Service {
  async generateInteger(): Promise<number> {
    this.server.logger.error("The random number generator is broken.");
    return 42;
  }

  async generateString(): Promise<string> {
    const { isProd, meta } = this.server.config();

    if (isProd) return "Don't Panic";
    return `${meta.name} is under development`;
  }
}
