/* eslint-disable camelcase */
import { SavedVoiceProvider } from "@app/db/build/main";
import Twilio from "twilio";
import { CallInstance } from "twilio/lib/rest/api/v2010/account/call";
import { Zammad, getOrCreateUser } from "./zammad";

export const twilioClientFor = (
  provider: SavedVoiceProvider
): Twilio.Twilio => {
  const { accountSid, apiKeySid, apiKeySecret } = provider.credentials;
  if (!accountSid || !apiKeySid || !apiKeySecret)
    throw new Error(
      `twilio provider ${provider.name} does not have credentials`
    );

  return Twilio(apiKeySid, apiKeySecret, {
    accountSid,
  });
};

export const createZammadTicket = async (
  call: CallInstance,
  mp3: Buffer
): Promise<void> => {
  const title = `Call from ${call.fromFormatted} at ${call.startTime}`;
  const body = `<ul>
    <li>Caller: ${call.fromFormatted}</li>
    <li>Service Number: ${call.toFormatted}</li>
    <li>Call Duration: ${call.duration} seconds</li>
    <li>Start Time: ${call.startTime}</li>
    <li>End Time: ${call.endTime}</li>
    </ul>
    <p>See the attached recording.</p>`;
  const filename = `${call.sid}-${call.startTime}.mp3`;
  const zammad = Zammad(
    {
      token: "EviH_WL0p6YUlCoIER7noAZEAPsYA_fVU4FZCKdpq525Vmzzvl8d7dNuP_8d-Amb",
    },
    "https://demo.digiresilience.org"
  );
  try {
    const customer = await getOrCreateUser(zammad, call.fromFormatted);
    await zammad.ticket.create({
      title,
      group: "Finances",
      note: "This ticket was created automaticaly from a recorded phone call.",
      customer_id: customer.id,
      article: {
        body,
        subject: title,
        content_type: "text/html",
        type: "note",
        attachments: [
          {
            filename,
            data: mp3.toString("base64"),
            "mime-type": "audio/mpeg",
          },
        ],
      },
    });
  } catch (error) {
    console.log(Object.keys(error));
    if (error.isBoom) {
      console.log(error.output);
      throw new Error("Failed to create zamamd ticket");
    }
  }
};
