/* eslint-disable camelcase */
import { withDb, AppDatabase } from "../db";
import workerUtils from "../utils";

interface WebhookPayload {
  to: string;
  from: string;
  message_id: string;
  sent_at: string;
  message: string;
  attachment: string;
  filename: string;
  mime_type: string;
}

interface SignalMessageTaskOptions {
  id: string;
  source: string;
  timestamp: string;
  message: string;
  attachments: unknown[];
  signalBotId: string;
}

const formatPayload = (
  messageInfo: SignalMessageTaskOptions
): WebhookPayload => {
  const { id, source, message, timestamp } = messageInfo;

  return {
    to: "16464229653",
    from: source,
    message_id: id,
    sent_at: timestamp,
    message,
    attachment: "",
    filename: "test.png",
    mime_type: "image/png",
  };
};

const notifyWebhooks = async (
  db: AppDatabase,
  messageInfo: SignalMessageTaskOptions
) => {
  const { id: messageID, signalBotId } = messageInfo;
  const webhooks = await db.webhooks.findAllByBackendId("signal", signalBotId);
  if (webhooks && webhooks.length === 0) return;

  webhooks.forEach(({ id }) => {
    const payload = formatPayload(messageInfo);
    console.log({ payload });
    workerUtils.addJob(
      "notify-webhook",
      {
        payload,
        webhookId: id,
      },
      {
        // this de-deduplicates the job
        jobKey: `webhook-${id}-message-${messageID}`,
      }
    );
  });
};

const signalMessageTask = async (
  options: SignalMessageTaskOptions
): Promise<void> => {
  console.log(options);
  withDb(async (db: AppDatabase) => {
    await notifyWebhooks(db, options);
  });
};

export default signalMessageTask;
