/* eslint-disable camelcase */
import fetch from "node-fetch";
import { URLSearchParams } from "url";
import { v4 as uuid } from "uuid";
import { Client } from "@opensearch-project/opensearch";
import { withDb, AppDatabase } from "../db";
import { loadConfig } from "@app/config";

type LabelStudioTicket = {
    id: string
    is_labeled: boolean
    annotations: Record<string, unknown>[]
    source_id: string
    source_created_at: string
    source_updated_at: string
}

type LeafcutterTicket = {
    id: string
    incident: string[]
    technology: string[]
    targeted_group: string[]
    country: string[]
    region: string[]
    continent: string[]
    date: Date
    origin_id: string
    source_id: string
    source_created_at: string
    source_updated_at: string
}

const getLabelStudioTickets = async (page: number): Promise<LabelStudioTicket[]> => {
    const {
        leafcutter: {
            labelStudioApiUrl,
            labelStudioApiKey,
        }
    } = await loadConfig();

    const headers = {
        Authorization: `Token ${labelStudioApiKey}`,
        Accept: "application/json",
    };
    const ticketsQuery = new URLSearchParams({
        page_size: "50",
        page: `${page}`,
    });
    const res = await fetch(`${labelStudioApiUrl}/projects/1/tasks?${ticketsQuery}`,
        { headers });
    const tasksResult = await res.json();

    return tasksResult;
}

const fetchFromLabelStudio = async (): Promise<LabelStudioTicket[]> => {
    const pages = [...Array.from({ length: 10000 }).keys()];
    const allDocs: LabelStudioTicket[] = [];

    for await (const page of pages) {
        const docs = await getLabelStudioTickets(page);

        if (docs && docs.length > 0) {
            allDocs.push(...docs)
        } else {
            break;
        }
    }

    return allDocs;
}

const sendToLeafcutter = async (tickets: LabelStudioTicket[]) => {
    const {
        leafcutter: {
            contributorId,
            opensearchApiUrl,
            opensearchUsername,
            opensearchPassword
        }
    } = await loadConfig();
    const client = new Client({
        node: `https://${opensearchUsername}:${opensearchPassword}@${opensearchApiUrl}`,
        ssl: {
            rejectUnauthorized: false,
        },
    });

    const createDocument = async (ticket: LeafcutterTicket) => {
        const res = await client.create({
            id: uuid(),
            index: "sample_tagged_tickets",
            refresh: true,
            body: ticket,
        });
        console.log(res);
    };

    const filteredTickets = tickets.filter((ticket) => ticket.is_labeled);

    const finalTickets: LeafcutterTicket[] = filteredTickets.map((ticket) => {
        const {
            id,
            annotations,
            source_id,
            source_created_at,
            source_updated_at
        } = ticket;

        const getTags = (tags: Record<string, any>[], name: string) =>
            tags
                .filter((tag) => tag.from_name === name)
                .map((tag) => tag.value.choices)
                .flat();

        const allTags = annotations.map(({ result }) => result).flat();
        const incident = getTags(allTags, "incidentType tag");
        const technology = getTags(allTags, "platform tag");
        const country = getTags(allTags, "country tag");
        const targetedGroup = getTags(allTags, "targetedGroup tag");

        return {
            id,
            incident,
            technology,
            targeted_group: targetedGroup,
            country,
            region: [],
            continent: [],
            date: new Date(source_created_at),
            origin_id: contributorId,
            source_id,
            source_created_at,
            source_updated_at
        };
    });

    for await (const ticket of finalTickets) {
        console.log(ticket);
        await createDocument(ticket);
    }
};


const importLeafcutterTask = async (): Promise<void> => {
    withDb(async (db: AppDatabase) => {
        console.log({ db });
        const tickets = await fetchFromLabelStudio();
        console.log({ tickets })
        await sendToLeafcutter(tickets);
    });
};

export default importLeafcutterTask;
