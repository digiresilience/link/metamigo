export const tagMap = {
    AccountImpersonation: [
        { field: "incidentType tag", value: "account-impersonation" },
    ],
    AppleID: [{ field: "incidentType tag", value: "malfunction-failure" }],
    Blocked: [{ field: "incidentType tag", value: "account-deactivation" }],
    CyberBullying: [{ field: "incidentType tag", value: "cyber-bullying" }],
    DeviceSuspiciousBehavior: [
        { field: "incidentType tag", value: "compromise-device" },
    ],
    Doxxing: [{ field: "incidentType tag", value: "doxxing" }],
    DSTips: [{ field: "incidentType tag", value: "informational" }],
    HackedLaptop: [
        { field: "incidentType tag", value: "compromised-device" },
        { field: "device tag", value: "laptop" },
    ],
    "Hacked/StolenAccount": [
        { field: "incidentType tag", value: "compromised-account" },
    ],
    HateSpeech: [{ field: "incidentType tag", value: "hate-speech" }],
    InfectedPhone: [
        { field: "incidentType tag", value: "malware" },
        { field: "device tag", value: "smartphone" },
    ],
    Kidnapping: [{ field: "incidentType tag", value: "kidnapping" }],
    LaptopGiveaway: [{ field: "incidentType tag", value: "other" }],
    ForensicAnalysis: [{ field: "incidentType tag", value: "malware" }],
    ISF: [{ field: "incidentType tag", value: "other" }],
    NumberBanned: [
        { field: "incidentType tag", value: "disruption" },
        { field: "device tag", value: "smartphone" },
    ],
    OnlineHarassment: [{ field: "incidentType tag", value: "online-harassment" }],
    PhoneHarassment: [{ field: "incidentType tag", value: "phone-harassment" }],
    PoliticalAds: [{ field: "incidentType tag", value: "spam" }],
    SeizedPhone: [
        { field: "incidentType tag", value: "confiscation" },
        { field: "device tag", value: "smartphone" },
    ],
    SexED: [{ field: "incidentType tag", value: "informational" }],
    Sextortion: [{ field: "incidentType tag", value: "sextortion" }],
    Spam: [{ field: "incidentType tag", value: "spam" }],
    SuspendedAccount: [
        { field: "incidentType tag", value: "account-suspension" },
    ],
    SuspendedActivities: [
        { field: "incidentType tag", value: "content-moderation" },
    ],
    SuspendedGroup: [{ field: "incidentType tag", value: "account-suspension" }],
    SuspendedPage: [{ field: "incidentType tag", value: "account-suspension" }],
    "Stolen/LostPhone": [
        { field: "incidentType tag", value: "loss" },
        { field: "device tag", value: "smartphone" },
    ],
    Facebook: [{ field: "platform tag", value: "facebook" }],
    Google: [{ field: "platform tag", value: "google" }],
    Instagram: [{ field: "platform tag", value: "instagram" }],
    SMS: [{ field: "service tag", value: "sms" }],
    Twitter: [{ field: "platform tag", value: "twitter" }],
    Website: [{ field: "service tag", value: "website" }],
    WhatsApp: [{ field: "platform tag", value: "whatsapp" }],
    YouTube: [{ field: "platform tag", value: "youtube" }],
    Linkedin: [{ field: "platform tag", value: "linkedin" }],
    PoliticalActivist: [{ field: "targetedGroup tag", value: "policy-politics" }],
    ElectoralCandidate: [
        { field: "targetedGroup tag", value: "policy-politics" },
    ],
    PhishingLink: [{ field: "incidentType tag", value: "phishing" }],
};
