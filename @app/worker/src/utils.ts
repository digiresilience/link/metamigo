import * as Worker from "graphile-worker";
import { defState } from "@digiresilience/montar";
import config from "@app/config";

const startWorkerUtils = async (): Promise<Worker.WorkerUtils> => {
  const workerUtils = await Worker.makeWorkerUtils({
    connectionString: config.worker.connection,
  });
  return workerUtils;
};

const stopWorkerUtils = async (): Promise<void> => {
  return workerUtils.release();
};

const workerUtils = defState("workerUtils", {
  start: startWorkerUtils,
  stop: stopWorkerUtils,
});

export default workerUtils;
