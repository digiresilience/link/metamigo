#!/bin/bash

set -eu

psql -Xv ON_ERROR_STOP=1 "${GM_DBURL}" <<EOF

INSERT INTO app_public.users(email, name, user_role, is_active, created_by)
VALUES('abel@guardianproject.info', 'Abel', 'admin'::app_public.role_type, true, 'afterCurrent Hook')
on conflict (email) do nothing;

INSERT INTO app_public.users(email, name, user_role, is_active, created_by)
VALUES('darren@redaranj.com', 'Darren', 'admin'::app_public.role_type, true, 'afterCurrent Hook')
on conflict (email) do nothing;

INSERT INTO app_public.users(email, name, user_role, is_active, created_by)
VALUES('jking@chambana.net', 'Josh', 'admin'::app_public.role_type, true, 'afterCurrent Hook')
on conflict (email) do nothing;

INSERT INTO app_public.settings(name, value)
VALUES('app-setting', to_jsonb('this is a setting value stored as json text'::text))
on conflict (name) do nothing;

EOF

if [[ -f "${PWD}/scripts/afterCurrent-private.sh" ]]; then
	# shellcheck source=/dev/null
	source "${PWD}/scripts/afterCurrent-private.sh"
fi
