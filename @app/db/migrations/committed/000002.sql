--! Previous: sha1:b13a5217288f5d349d8d9e3afbd7bb30c0dbad21
--! Hash: sha1:8659f815ff013a793f2e01113a9a61a98c7bd8d5

-- Enter migration here

drop table if exists app_public.whatsapp_attachments cascade;
drop table if exists app_public.whatsapp_messages cascade;

grant delete on app_public.whatsapp_bots to app_admin;
grant delete on app_public.signal_bots to app_admin;
