import {
  RepositoryBase,
  recordInfo,
  UUID,
  Flavor,
} from "@digiresilience/amigo";

export type WhatsappMessageId = Flavor<UUID, "Whatsapp Message Id">;

export interface UnsavedWhatsappMessage {
  whatsappBotId: string;
  waMessageId: string;
  waTimestamp: Date;
  waMessage: string;
  attachments?: string[];
}

export interface SavedWhatsappMessage extends UnsavedWhatsappMessage {
  id: WhatsappMessageId;
  createdAt: Date;
  updatedAt: Date;
}

export const WhatsappMessageRecord = recordInfo<
  UnsavedWhatsappMessage,
  SavedWhatsappMessage
>("app_public", "whatsapp_messages");

export class WhatsappMessageRecordRepository extends RepositoryBase(
  WhatsappMessageRecord
) {}
