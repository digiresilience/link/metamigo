import {
  RepositoryBase,
  recordInfo,
  UUID,
  Flavor,
} from "@digiresilience/amigo";

export type WhatsappAttachmentId = Flavor<UUID, "Whatsapp Attachment Id">;

export interface UnsavedWhatsappAttachment {
  whatsappBotId: string;
  whatsappMessageId: string;
  attachment: Buffer;
}

export interface SavedWhatsappAttachment extends UnsavedWhatsappAttachment {
  id: WhatsappAttachmentId;
  createdAt: Date;
  updatedAt: Date;
}

export const WhatsappAttachmentRecord = recordInfo<
  UnsavedWhatsappAttachment,
  SavedWhatsappAttachment
>("app_public", "whatsapp_attachments");

export class WhatsappAttachmentRecordRepository extends RepositoryBase(
  WhatsappAttachmentRecord
) {}
