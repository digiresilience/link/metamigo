import {
  RepositoryBase,
  recordInfo,
  UUID,
  Flavor,
} from "@digiresilience/amigo";

export type SignalBotId = Flavor<UUID, "Signal Bot Id">;

export interface UnsavedSignalBot {
  phoneNumber: string;
  userId: string;
  description: string;
}

export interface SavedSignalBot extends UnsavedSignalBot {
  id: SignalBotId;
  createdAt: Date;
  updatedAt: Date;
  token: string;
  authInfo: string;
  isVerified: boolean;
}

export const SignalBotRecord = recordInfo<UnsavedSignalBot, SavedSignalBot>(
  "app_public",
  "signal_bots"
);

export class SignalBotRecordRepository extends RepositoryBase(SignalBotRecord) {
  async updateAuthInfo(
    bot: SavedSignalBot,
    authInfo: string | undefined
  ): Promise<SavedSignalBot> {
    return this.db.one(
      "UPDATE $1 SET (auth_info, is_verified) = ROW($2, true) WHERE id = $3 RETURNING *",
      [this.schemaTable, authInfo, bot.id]
    );
  }
}
