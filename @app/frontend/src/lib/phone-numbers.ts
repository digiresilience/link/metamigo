import { regex } from "react-admin";

export const E164Regex = /^\+[1-9]\d{1,14}$/;
/**
 * Returns true if the number is a valid E164 number
 */
export const isValidE164Number = (phoneNumber) => {
  return E164Regex.test(phoneNumber);
};

/**
 * Given a phone number approximation, will clean out whitespace and punctuation.
 */
export const sanitizeE164Number = (phoneNumber) => {
  if (!phoneNumber) return "";
  if (!phoneNumber.trim()) return "";
  const sanitized = phoneNumber
    .replace(/\s/g, "")
    .replace(/\./g, "")
    .replace(/-/g, "")
    .replace(/\(/g, "")
    .replace(/\)/g, "");

  if (sanitized[0] !== "+") return `+${sanitized}`;
  return sanitized;
};

export const validateE164Number = regex(
  E164Regex,
  "Must start with a + and have no punctunation and no spaces."
);
