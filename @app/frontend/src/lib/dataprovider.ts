import pgDataProvider from "ra-postgraphile";
import schema from "./graphql-schema.json";

export const amigoDataProvider = async (client) => {
  const graphqlDataProvider = await pgDataProvider(
    client,
    // @ts-expect-error: Missing property
    {},
    { introspection: { schema: schema.data.__schema } }
  );

  const dataProvider = async (type, resource, params) => {
    return graphqlDataProvider(type, resource, params);
  };

  return dataProvider;
};
