import "../styles/globals.css";
import { Provider } from "next-auth/client";

function AmigoStarter({ Component, pageProps }) {
  return (
    <Provider session={pageProps.session}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default AmigoStarter;
