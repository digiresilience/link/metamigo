import { ApolloProvider } from "@apollo/client";
import { apolloClient } from "../lib/apollo-client";
import dynamic from "next/dynamic";

const AmigoAdmin = dynamic(() => import("../components/AmigoAdmin"), {
  ssr: false,
});

export default function Home() {
  return (
    <ApolloProvider client={apolloClient}>
      <AmigoAdmin />
    </ApolloProvider>
  );
}
