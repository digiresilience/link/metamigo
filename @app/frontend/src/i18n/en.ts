import { TranslationMessages } from "react-admin";
import englishMessages from "ra-language-english";

const customEnglishMessages: TranslationMessages = {
  ...englishMessages,

  auth: {
    loggingIn: "Logging in...",
    permissionDenied: "Permission denied",
  },
  pos: {
    configuration: "Configuration",
    menu: {
      security: "Security",
      accounts: "Accounts",
      voicelines: "Voice Lines",
      providers: "Voice Provider",
      webhooks: "Webhooks",
      voice: "Voice",
      whatsapp: "WhatsApp",
      signal: "Signal",
    },
  },
  resources: {
    signalBots: {
      name: "Signal Bot |||| Signal Bots",
      verifyDialog: {
        sms:
          "Please enter the verification code sent via SMS to %{phoneNumber}",
        voice:
          "Please answer the call from Signal to %{phoneNumber} and enter the verification code",
      },
    },
    whatsappBots: {
      name: "WhatsApp Bot |||| WhatsApp Bots",
    },
    users: {
      name: "User |||| Users",
    },
    accounts: {
      name: "OAuth Account |||| OAuth Accounts",
    },
    voicelines: {
      name: "Voice Line |||| Voice Lines",
      fields: {
        providerLineSid: "Provider Line SID",
      },
    },
    providers: {
      name: "Voice Provider |||| Voice Providers",
      fields: {
        credentials: {
          accountSid: "Twilio Account SID",
          apiKeySid: "Twilio API Key SID",
          apiKeySecret: "Twilio API Key Secret",
        },
      },
    },
    webhooks: {
      name: "Webhook |||| Webhooks",
      fields: {
        endpointUrl: "Endpoint URL",
        httpMethod: "HTTP Method",
        headers: "HTTP Headers",
        header: "Header Name",
        value: "Header Value",
      },
    },
  },
  validation: {
    url: "a valid url starting with https:// is required",
    voice: "a voice is required",
    language: "a language is required",
    headerName: "a valid http header name has only letters, numbers and dashes",
    noAvailableNumbers:
      "There are no available numbers to assign. Please visit the provider and purchase more numbers.",
    noVoiceLines:
      "There are no configured voice lines. Visit the Voice Lines admin page to create some.",
  },
};

export default customEnglishMessages;
