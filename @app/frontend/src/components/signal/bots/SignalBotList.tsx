import React from "react";
import {
  List,
  Datagrid,
  DateField,
  TextField,
  BooleanField,
  ListProps,
} from "react-admin";

const SignalBotList = (props: ListProps) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="show">
      <TextField source="phoneNumber" />
      <TextField source="description" />
      <BooleanField source="isVerified" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <TextField source="createdBy" />
    </Datagrid>
  </List>
);

export default SignalBotList;
