import {
  SelectInput,
  required,
  ReferenceInput,
  ReferenceField,
  TextField,
} from "react-admin";

export const SignalBotSelectInput = (source: string) => () => (
  <ReferenceInput
    label="Signal Bot"
    source={source}
    reference="signalBots"
    validate={[required()]}
  >
    <SelectInput optionText="phoneNumber" />
  </ReferenceInput>
);

export const SignalBotField = (source: string) => () => (
  <ReferenceField label="Signal Bot" reference="signalBots" source={source}>
    <TextField source="phoneNumber" />
  </ReferenceField>
);
