import SignalBotIcon from "@material-ui/icons/ChatOutlined";
import SignalBotList from "./SignalBotList";
import SignalBotEdit from "./SignalBotEdit";
import SignalBotCreate from "./SignalBotCreate";
import SignalBotShow from "./SignalBotShow";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: SignalBotList,
  create: SignalBotCreate,
  edit: SignalBotEdit,
  show: SignalBotShow,
  icon: SignalBotIcon,
};
