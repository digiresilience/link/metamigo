import React from "react";
import { SimpleForm, Edit, TextInput, required, EditProps } from "react-admin";

const SignalBotEdit = (props: EditProps) => (
  <Edit {...props} title="Edit Bot">
    <SimpleForm>
      <TextInput disabled source="phoneNumber" validate={[required()]} />
      <TextInput source="description" />
    </SimpleForm>
  </Edit>
);

export default SignalBotEdit;
