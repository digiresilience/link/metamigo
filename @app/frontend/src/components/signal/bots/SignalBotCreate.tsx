import React from "react";
import {
  SimpleForm,
  Create,
  TextInput,
  required,
  CreateProps,
} from "react-admin";
import { useSession } from "next-auth/client";
import { validateE164Number } from "../../../lib/phone-numbers";

const SignalBotCreate = (props: CreateProps) => {
  const [session] = useSession();

  return (
    <Create {...props} title="Create Signal Bot">
      <SimpleForm>
        <TextInput
          source="userId"
          defaultValue={
            // @ts-expect-error: ID does exist
            session.user.id
          }
        />
        <TextInput
          source="phoneNumber"
          validate={[validateE164Number, required()]}
        />
        <TextInput source="description" />
      </SimpleForm>
    </Create>
  );
};

export default SignalBotCreate;
