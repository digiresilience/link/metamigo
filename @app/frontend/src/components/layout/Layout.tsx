import * as React from "react";
import { Layout as RaLayout, LayoutProps, Sidebar } from "react-admin";
import AppBar from "./AppBar";
import Menu from "./Menu";
import { theme } from "./themes";

const CustomSidebar = (props: any) => <Sidebar {...props} size={200} />;

const Layout = (props: LayoutProps) => {
  return (
    <RaLayout
      {...props}
      appBar={AppBar}
      menu={Menu}
      sidebar={CustomSidebar}
      theme={theme}
    />
  );
};

export default Layout;
