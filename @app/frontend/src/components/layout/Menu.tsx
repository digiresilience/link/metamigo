/* eslint-disable camelcase */
import * as React from "react";
import { FC, useState } from "react";
import { useSelector } from "react-redux";
import SecurityIcon from "@material-ui/icons/Security";
import VoiceIcon from "@material-ui/icons/PhoneInTalk";
import Box from "@material-ui/core/Box";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTranslate, MenuItemLink, MenuProps, ReduxState } from "react-admin";
import users from "../users";
import accounts from "../accounts";
import webhooks from "../webhooks";
import voiceLines from "../voice/voicelines";
import voiceProviders from "../voice/providers";
import whatsappBots from "../whatsapp/bots";
import signalBots from "../signal/bots";
import SubMenu from "./SubMenu";

type MenuName = "menuVoice" | "menuSecurity";

const Menu: FC<MenuProps> = ({ onMenuClick, logout, dense = false }) => {
  const [state, setState] = useState({
    menuVoice: false,
    menuSecurity: false,
  });
  const translate = useTranslate();
  const theme = useTheme();
  const isXSmall = useMediaQuery(theme.breakpoints.down("xs"));
  const open = useSelector((state: ReduxState) => state.admin.ui.sidebarOpen);
  useSelector((state: ReduxState) => state.theme); // force rerender on theme change

  const handleToggle = (menu: MenuName) => {
    setState((state) => ({ ...state, [menu]: !state[menu] }));
  };

  return (
    <Box mt={1}>
      <MenuItemLink
        to={`/whatsappbots`}
        primaryText={translate(`pos.menu.whatsapp`, {
          smart_count: 2,
        })}
        leftIcon={<whatsappBots.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <MenuItemLink
        to={`/signalbots`}
        primaryText={translate(`pos.menu.signal`, {
          smart_count: 2,
        })}
        leftIcon={<signalBots.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <SubMenu
        handleToggle={() => handleToggle("menuVoice")}
        isOpen={state.menuVoice}
        sidebarIsOpen={open}
        name="pos.menu.voice"
        icon={<VoiceIcon />}
        dense={dense}
      >
        <MenuItemLink
          to={`/voiceproviders`}
          primaryText={translate(`resources.providers.name`, {
            smart_count: 2,
          })}
          leftIcon={<voiceProviders.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/voicelines`}
          primaryText={translate(`resources.voicelines.name`, {
            smart_count: 2,
          })}
          leftIcon={<voiceLines.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      <MenuItemLink
        to={`/webhooks`}
        primaryText={translate(`resources.webhooks.name`, {
          smart_count: 2,
        })}
        leftIcon={<webhooks.icon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <SubMenu
        handleToggle={() => handleToggle("menuSecurity")}
        isOpen={state.menuSecurity}
        sidebarIsOpen={open}
        name="pos.menu.security"
        icon={<SecurityIcon />}
        dense={dense}
      >
        <MenuItemLink
          to={`/users`}
          primaryText={translate(`resources.users.name`, {
            smart_count: 2,
          })}
          leftIcon={<users.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
        <MenuItemLink
          to={`/accounts`}
          primaryText={translate(`resources.accounts.name`, {
            smart_count: 2,
          })}
          leftIcon={<accounts.icon />}
          onClick={onMenuClick}
          sidebarIsOpen={open}
          dense={dense}
        />
      </SubMenu>
      {isXSmall && logout}
    </Box>
  );
};

export default Menu;
