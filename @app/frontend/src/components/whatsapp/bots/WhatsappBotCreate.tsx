import React from "react";
// import dynamic from "next/dynamic";
import { SimpleForm, Create, TextInput, required } from "react-admin";
import { useSession } from "next-auth/client";
import { validateE164Number } from "../../../lib/phone-numbers";

const WhatsappBotCreate = (props) => {
  // const MuiPhoneNumber = dynamic(() => import("material-ui-phone-number"), {
  //   ssr: false,
  // });

  const [session] = useSession();

  return (
    <Create {...props} title="Create Whatsapp Bot" redirect="show">
      <SimpleForm>
        <TextInput
          source="userId"
          defaultValue={
            // @ts-expect-error: non-existent property
            session.user.id
          }
        />
        <TextInput
          source="phoneNumber"
          validate={[validateE164Number, required()]}
        />
        {/* <MuiPhoneNumber
          defaultCountry={"us"}
          fullWidth
          onChange={(e: any) => setFieldValue("phoneNumber", e)}
        /> */}
        <TextInput source="description" />
      </SimpleForm>
    </Create>
  );
};

export default WhatsappBotCreate;
