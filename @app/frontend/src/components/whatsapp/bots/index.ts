import WhatsappBotIcon from "@material-ui/icons/WhatsApp";
import WhatsappBotList from "./WhatsappBotList";
import WhatsappBotEdit from "./WhatsappBotEdit";
import WhatsappBotCreate from "./WhatsappBotCreate";
import WhatsappBotShow from "./WhatsappBotShow";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: WhatsappBotList,
  create: WhatsappBotCreate,
  edit: WhatsappBotEdit,
  show: WhatsappBotShow,
  icon: WhatsappBotIcon,
};
