import {
  SelectInput,
  required,
  ReferenceInput,
  ReferenceField,
  TextField,
} from "react-admin";

export const WhatsAppBotSelectInput = (source: string) => () => (
  <ReferenceInput
    label="WhatsApp Bot"
    reference="whatsappBots"
    source={source}
    validate={[required()]}
  >
    <SelectInput optionText="phoneNumber" />
  </ReferenceInput>
);

export const WhatsAppBotField = (source: string) => () => (
  <ReferenceField label="WhatsApp Bot" reference="whatsappBots" source={source}>
    <TextField source="phoneNumber" />
  </ReferenceField>
);
