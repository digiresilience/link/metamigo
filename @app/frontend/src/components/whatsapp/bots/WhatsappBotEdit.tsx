import React from "react";
import { SimpleForm, Edit, TextInput, required, EditProps } from "react-admin";

const WhatsappBotEdit = (props: EditProps) => (
  <Edit {...props} title="Edit Bot">
    <SimpleForm>
      <TextInput source="phoneNumber" validate={[required()]} />
      <TextInput source="description" />
    </SimpleForm>
  </Edit>
);

export default WhatsappBotEdit;
