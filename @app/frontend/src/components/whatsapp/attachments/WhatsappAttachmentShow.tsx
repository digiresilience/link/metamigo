import React from "react";
import { Show, ShowProps, SimpleShowLayout, TextField } from "react-admin";

const WhatsappAttachmentShow = (props: ShowProps) => (
  <Show {...props} title="Whatsapp Attachment">
    <SimpleShowLayout>
      <TextField source="id" />
    </SimpleShowLayout>
  </Show>
);

export default WhatsappAttachmentShow;
