import WhatsappAttachmentIcon from "@material-ui/icons/AttachFile";
import WhatsappAttachmentList from "./WhatsappAttachmentList";
import WhatsappAttachmentShow from "./WhatsappAttachmentShow";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: WhatsappAttachmentList,
  show: WhatsappAttachmentShow,
  icon: WhatsappAttachmentIcon,
};
