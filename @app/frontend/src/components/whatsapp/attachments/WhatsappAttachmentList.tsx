import React from "react";
import { List, Datagrid, TextField } from "react-admin";

const WhatsappAttachmentList = (props) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="show">
      <TextField source="id" />
    </Datagrid>
  </List>
);

export default WhatsappAttachmentList;
