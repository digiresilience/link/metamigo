import React from "react";
import {
  List,
  ListProps,
  Datagrid,
  DateField,
  TextField,
  BooleanField,
} from "react-admin";

const WhatsappMessageList = (props: ListProps) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="show">
      <TextField source="phoneNumber" />
      <TextField source="description" />
      <BooleanField source="isVerified" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <TextField source="createdBy" />
    </Datagrid>
  </List>
);

export default WhatsappMessageList;
