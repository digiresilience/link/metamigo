import WhatsappMessageIcon from "@material-ui/icons/Message";
import WhatsappMessageList from "./WhatsappMessageList";
import WhatsappMessageShow from "./WhatsappMessageShow";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: WhatsappMessageList,
  show: WhatsappMessageShow,
  icon: WhatsappMessageIcon,
};
