import React from "react";
import {
  Show,
  ShowProps,
  SimpleShowLayout,
  TextField,
  ReferenceManyField,
  Datagrid,
} from "react-admin";

const WhatsappMessageShow = (props: ShowProps) => (
  <Show {...props} title="Whatsapp Message">
    <SimpleShowLayout>
      <TextField source="waMessage" />
      <TextField source="createdAt" />
      <ReferenceManyField
        label="Attachments"
        reference="whatsappAttachments"
        target="whatsappMessageId"
      >
        <Datagrid rowClick="show">
          <TextField source="id" />
          <TextField source="createdAt" />
        </Datagrid>
      </ReferenceManyField>
    </SimpleShowLayout>
  </Show>
);

export default WhatsappMessageShow;
