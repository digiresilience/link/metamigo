import React from "react";
import useDigitInput, { InputAttributes } from "react-digit-input";
import styles from "./DigitInput.module.css";

const DigitInputElement = React.forwardRef<
  HTMLInputElement,
  Omit<InputAttributes, "ref"> & {
    autoFocus?: boolean;
  }
>(({ ...props }, ref) => {
  return (
    <>
      <input
        aria-label="verification code"
        className={styles.input}
        {...props}
        ref={ref}
        inputMode="decimal"
      />
    </>
  );
});

const DigitSeparator = React.forwardRef<
  HTMLInputElement,
  Omit<InputAttributes, "ref"> & {
    autoFocus?: boolean;
  }
>(({ ...props }, ref) => {
  return (
    <>
      <span className={styles.hyphen} ref={ref} />
    </>
  );
});

export const SixDigitInput = ({ value, onChange }) => {
  const digits = useDigitInput({
    acceptedCharacters: /^[0-9]$/,
    length: 6,
    value,
    onChange,
  });
  return (
    <div>
      <div className={styles.group}>
        <DigitInputElement autoFocus {...digits[0]} />
        <DigitInputElement {...digits[1]} />
        <DigitInputElement {...digits[2]} />
        <DigitSeparator />
        <DigitInputElement {...digits[3]} />
        <DigitInputElement {...digits[4]} />
        <DigitInputElement {...digits[5]} />
      </div>
      <pre hidden>
        <code>{value}</code>
      </pre>
    </div>
  );
};
