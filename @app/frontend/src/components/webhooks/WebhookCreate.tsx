import React from "react";
import {
  SimpleForm,
  FormDataConsumer,
  TextInput,
  Create,
  ArrayInput,
  SimpleFormIterator,
  regex,
  required,
  CreateProps,
} from "react-admin";
import { BackendTypeInput, BackendIdInput, HttpMethodInput } from "./shared";
/*


        <ReferenceInput
          label="Voice Line"
          source="voiceLineId"
          reference="voiceLines"
          validate={[required()]}
        >
          <SelectInput optionText="number" />
        </ReferenceInput>
*/
const WebhookCreate = (props: CreateProps) => {
  return (
    <Create {...props} title="Create Webhooks">
      <SimpleForm>
        <TextInput source="name" validate={[required()]} />
        <BackendTypeInput />
        <FormDataConsumer subscription={{ values: true }}>
          {BackendIdInput}
        </FormDataConsumer>
        <TextInput
          source="endpointUrl"
          validate={[required(), regex(/^https?:\/\/[^/]+/, "validation.url")]}
        />
        <HttpMethodInput />
        <ArrayInput source="headers">
          <SimpleFormIterator>
            <TextInput
              source="header"
              validate={[
                required(),
                regex(/^[\w-]+$/, "validation.headerName"),
              ]}
            />
            <TextInput source="value" validate={[required()]} />
          </SimpleFormIterator>
        </ArrayInput>
      </SimpleForm>
    </Create>
  );
};

export default WebhookCreate;
