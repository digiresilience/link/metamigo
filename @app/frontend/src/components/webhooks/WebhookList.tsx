import React from "react";
import {
  List,
  Datagrid,
  DateField,
  TextField,
  ReferenceField,
  ListProps,
} from "react-admin";
import { BackendIdField } from "./shared";

const WebhookList = (props: ListProps) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <TextField source="name" />
      <TextField source="backendType" />
      <BackendIdField source="backendId" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default WebhookList;
