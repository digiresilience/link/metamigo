import React from "react";
import { SelectInput, required } from "react-admin";

import {
  VoiceLineField,
  VoiceLineSelectInput,
} from "../voice/voicelines/shared";
import {
  WhatsAppBotField,
  WhatsAppBotSelectInput,
} from "../whatsapp/bots/shared";
import { SignalBotField, SignalBotSelectInput } from "../signal/bots/shared";

const httpChoices = [
  { id: "post", name: "POST" },
  { id: "put", name: "PUT" },
];
export const HttpMethodInput = (props) => (
  <SelectInput
    source="httpMethod"
    choices={httpChoices}
    validate={[required()]}
    initialValue="post"
    {...props}
  />
);

const backendChoices = [
  { id: "signal", name: "Signal" },
  { id: "whatsapp", name: "WhatsApp" },
  { id: "voice", name: "Voice" },
];

const backendInputComponents = {
  whatsapp: WhatsAppBotSelectInput("backendId"),
  signal: SignalBotSelectInput("backendId"),
  voice: VoiceLineSelectInput("backendId"),
};

const backendFieldComponents = {
  whatsapp: WhatsAppBotField("backendId"),
  signal: SignalBotField("backendId"),
  voice: VoiceLineField("backendId"),
};

export const BackendTypeInput = (props) => (
  <SelectInput
    source="backendType"
    choices={backendChoices}
    validate={[required()]}
    {...props}
  />
);

export const BackendIdInput = (form, ...rest) => {
  const Component = form.formData.backendType
    ? backendInputComponents[form.formData.backendType]
    : false;
  return <>{Component && <Component form={form} {...rest} />}</>;
};

export const BackendIdField = (form, ...rest) => {
  console.log(form);

  const Component = form.record.backendType
    ? backendFieldComponents[form.record.backendType]
    : false;
  return <>{Component && <Component form={form} {...rest} />}</>;
};
