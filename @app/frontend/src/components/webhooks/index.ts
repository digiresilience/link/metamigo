import WebhookIcon from "@material-ui/icons/Send";
import WebhookList from "./WebhookList";
import WebhookEdit from "./WebhookEdit";
import WebhookCreate from "./WebhookCreate";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: WebhookList,
  create: WebhookCreate,
  edit: WebhookEdit,
  icon: WebhookIcon,
};
