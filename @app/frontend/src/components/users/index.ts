import UserIcon from "@material-ui/icons/People";
import UserList from "./UserList";
import UserEdit from "./UserEdit";
import UserCreate from "./UserCreate";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: UserList,
  create: UserCreate,
  edit: UserEdit,
  icon: UserIcon,
};
