import { SelectInput } from "react-admin";

export const UserRoleInput = (props) => (
  <SelectInput
    source="userRole"
    choices={[
      { id: "NONE", name: "None" },
      { id: "USER", name: "User" },
      { id: "ADMIN", name: "Admin" },
    ]}
    disabled={props.session.user.id === props.record.id}
    {...props}
  />
);
