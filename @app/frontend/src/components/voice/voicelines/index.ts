import VoiceLineIcon from "@material-ui/icons/PhoneCallback";
import VoiceLineList from "./VoiceLineList";
import VoiceLineEdit from "./VoiceLineEdit";
import VoiceLineCreate from "./VoiceLineCreate";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: VoiceLineList,
  create: VoiceLineCreate,
  edit: VoiceLineEdit,
  icon: VoiceLineIcon,
};
