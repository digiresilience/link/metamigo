import React from "react";
import {
  SimpleForm,
  Create,
  FormDataConsumer,
  SelectInput,
  BooleanInput,
  ReferenceInput,
  required,
  CreateProps,
} from "react-admin";
import TwilioLanguages from "./twilio-languages";
import {
  PromptInput,
  VoiceInput,
  AvailableNumbersInput,
  populateNumber,
} from "./shared";
import MicInput from "./MicInput";

const VoiceLineCreate = (props: CreateProps) => {
  return (
    <Create {...props} title="Create Voice Line" transform={populateNumber}>
      <SimpleForm>
        <ReferenceInput
          label="Provider"
          source="providerId"
          reference="voiceProviders"
          validate={[required()]}
        >
          <SelectInput optionText={(p) => `${p.kind}: ${p.name}`} />
        </ReferenceInput>
        <FormDataConsumer subscription={{ values: true }}>
          {AvailableNumbersInput}
        </FormDataConsumer>
        <SelectInput
          source="language"
          choices={TwilioLanguages.languages}
          validate={[required()]}
        />
        <FormDataConsumer subscription={{ values: true }}>
          {VoiceInput}
        </FormDataConsumer>

        <FormDataConsumer subscription={{ values: true }}>
          {PromptInput}
        </FormDataConsumer>
        <BooleanInput source="audioPromptEnabled" />
        <MicInput source="promptAudio" />
      </SimpleForm>
    </Create>
  );
};

export default VoiceLineCreate;
