import React from "react";
import {
  SimpleForm,
  TextInput,
  Edit,
  FormDataConsumer,
  SelectInput,
  BooleanInput,
  ReferenceInput,
  required,
  EditProps,
} from "react-admin";
import TwilioLanguages from "./twilio-languages";
import { VoiceInput, PromptInput } from "./shared";
import MicInput from "./MicInput";

const VoiceLineTitle = ({ record }: { record?: any }) => {
  let title = "";
  if (record) title = record.name ? record.name : record.email;
  return <span>VoiceLine {title}</span>;
};

const VoiceLineEdit = (props: EditProps) => {
  return (
    <Edit title={<VoiceLineTitle />} {...props}>
      <SimpleForm>
        <ReferenceInput
          disabled
          label="Provider"
          source="providerId"
          reference="providers"
          validate={[required()]}
        >
          <SelectInput optionText={(p) => `${p.kind}: ${p.name}`} />
        </ReferenceInput>
        <TextInput disabled source="providerLineSid" />
        <TextInput disabled source="number" />
        <SelectInput source="language" choices={TwilioLanguages.languages} />
        <FormDataConsumer subscription={{ values: true }}>
          {VoiceInput}
        </FormDataConsumer>
        <FormDataConsumer subscription={{ values: true }}>
          {PromptInput}
        </FormDataConsumer>
        <BooleanInput source="audioPromptEnabled" />
        <MicInput source="promptAudio" />
      </SimpleForm>
    </Edit>
  );
};

export default VoiceLineEdit;
