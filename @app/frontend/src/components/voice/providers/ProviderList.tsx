import React from "react";
import { List, Datagrid, DateField, TextField, ListProps } from "react-admin";

const ProviderList = (props: ListProps) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <TextField source="kind" />
      <TextField source="name" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </Datagrid>
  </List>
);

export default ProviderList;
