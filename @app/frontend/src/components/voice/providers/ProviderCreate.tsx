import React from "react";
import {
  SimpleForm,
  TextInput,
  Create,
  PasswordInput,
  CreateProps,
} from "react-admin";
import { ProviderKindInput } from "./shared";

import TextField from "@material-ui/core/TextField";

const TwilioCredentialsInput = () => (
  <span>
    <TextField name="accountSid" label="Account Sid" />
    <TextField name="authToken" label="Auth Token" />
  </span>
);

const ProviderCreate = (props: CreateProps) => {
  return (
    <Create {...props} title="Create Providers">
      <SimpleForm>
        <ProviderKindInput />
        <TextInput source="name" />
        <TextInput source="credentials.accountSid" />
        <TextInput source="credentials.apiKeySid" />
        <PasswordInput source="credentials.apiKeySecret" />
      </SimpleForm>
    </Create>
  );
};

export default ProviderCreate;
