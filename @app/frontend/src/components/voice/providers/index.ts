import ProviderIcon from "@material-ui/icons/Business";
import ProviderList from "./ProviderList";
import ProviderEdit from "./ProviderEdit";
import ProviderCreate from "./ProviderCreate";

export default {
  list: ProviderList,
  create: ProviderCreate,
  edit: ProviderEdit,
  icon: ProviderIcon,
};
