import React from "react";
import {
  SimpleForm,
  TextInput,
  PasswordInput,
  Edit,
  EditProps,
} from "react-admin";
import { ProviderKindInput } from "./shared";

const ProviderTitle = ({ record }: { record?: any }) => {
  let title = "";
  if (record) title = record.name ? record.name : record.email;
  return <span>Provider {title}</span>;
};

const ProviderEdit = (props: EditProps) => {
  return (
    <Edit title={<ProviderTitle />} {...props}>
      <SimpleForm>
        <TextInput disabled source="id" />
        <ProviderKindInput disabled />
        <TextInput source="name" />
        <TextInput source="credentials.accountSid" />
        <TextInput source="credentials.apiKeySid" />
        <PasswordInput source="credentials.apiKeySecret" />
      </SimpleForm>
    </Edit>
  );
};

export default ProviderEdit;
