import AccountIcon from "@material-ui/icons/AccountTree";
import AccountList from "./AccountList";
import AccountEdit from "./AccountEdit";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  list: AccountList,
  edit: AccountEdit,
  icon: AccountIcon,
};
