import React from "react";
import {
  List,
  Datagrid,
  DateField,
  TextField,
  ReferenceField,
  DeleteButton,
  ListProps,
} from "react-admin";
import { useSession } from "next-auth/client";

const DeleteNotSelfButton = (props) => {
  const [session] = useSession();
  return (
    <DeleteButton
      disabled={session.user.email === props.record.userId}
      {...props}
    />
  );
};

export const AccountList = (props: ListProps) => (
  <List {...props} exporter={false}>
    <Datagrid rowClick="edit">
      <ReferenceField source="userId" reference="users">
        <TextField source="email" />
      </ReferenceField>
      <TextField source="providerType" />
      <TextField source="providerId" />
      <TextField source="providerAccountId" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <DeleteNotSelfButton />
    </Datagrid>
  </List>
);

export default AccountList;
